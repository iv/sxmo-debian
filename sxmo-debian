#!/bin/sh
#
# Build and install git version of SXMO onto a Mobian image.
# If run again it can be used to update the SXMO git packages and rebuild them without
# reinstalling everything else.
#
# Based on https://github.com/justinesmithies/sxmo-alarm by Justine Smithies
# IV - 21/08/2021

if ! [ $(id -u) = 0 ]; then
   echo "The script needs to be run as root." >&2
   exit 1
fi

if [ $SUDO_USER ]; then
    real_user=$SUDO_USER
else
    real_user=$(whoami)
fi

# Commands that you don't want running as root would be invoked
# with: sudo -u $real_user
# So they will be run as the user who invoked the sudo command
# Keep in mind if the user is using a root shell (they're logged in as root),
# then $real_user is actually root
# sudo -u $real_user non-root-command

# Commands that need to be ran with root would be invoked without sudo
# root-command

sudo -u $real_user mkdir -p build-sxmo-dev || exit 1

cd build-sxmo-dev || exit 1

echo "Updating Debian.">&2

apt-get update

echo "Installing build dependencies.">&2
apt-get -y install xorg libxft2 libxinerama1 libwebkit2gtk-4.0 linux-headers-arm64 fontconfig libfreetype6 libxext6 libinput10 libncursesw5 git bc busybox  xdg-user-dirs cmake libicu67 libphonenumber8 libgo14
apt-get -y install xcalib x11-xserver-utils xdm x11-utils xdotool xsel xinput x11-apps xclip xserver-xorg-input-synaptics xserver-xorg-video-fbdev
apt-get -y install wget unzip fonts-dejavu alsa-utils autocutsel inotify-tools sxiv vis pulseaudio conky megapixels feh dunst modemmanager ffmpeg geoclue-2.0 mediainfo
apt-get -y install firefox-esr-mobile-config xfonts-terminus highlight xcursor-themes youtube-dl screen mpv dnsmasq libsdl2-2.0-0 bluez scrot
apt-get -y install libx11-dev x11proto-core-dev libxt-dev libx11-xcb-dev libxcb-res0-dev libinput-dev libsdl2-dev libphonenumber-dev gccgo golang-go libxml2-utils mc htop

# Install Fira Mono fonts                                                                                                                                                                   
mkdir -p /usr/share/fonts/OTF                                                                                                                                                               
sudo -u $real_user mkdir -p /home/$real_user/build-sxmo-dev/FiraMono                                                                                                                        
cd /home/$real_user/build-sxmo-dev/FiraMono                                                                                                                                                 
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraMono.zip                                                                                                          
sudo -u $real_user unzip -o FiraMono.zip                                                                                                                                                    
cp -f *.otf /usr/share/fonts/OTF                                                                                                                                                            
sudo -u $real_user fc-cache                                                                                                                                                                 
rm *.zip                                                                                                                                                                                    
cd /home/$real_user/build-sxmo-dev


# Create sxmo-pinephone.service

if [ ! -f /etc/systemd/system/sxmo-pinephone.service ]; then
    cat > "/etc/systemd/system/sxmo-pinephone.service" <<EOF
[Unit]
Description=sxmo-pinephone

[Service]
ExecStart=/usr/bin/sxmo_setpermissions.sh

[Install]
WantedBy=multi-user.target
EOF
fi

# Fetch and build required SXMO packages

repos="~mil/sxmo-utils ~mil/sxmo-dwm ~mil/sxmo-dmenu ~mil/lisgd ~mil/sxmo-surf ~mil/sxmo-st ~proycon/svkbd ~proycon/clickclack ~mil/sxmo-xdm-config Orange-OpenSource/pn ~anjan/mnc"

for repo in $repos; do
	echo "Obtaining $repo...">&2
	repodir="$(basename "$repo")"
	if [ -d "$repodir" ]; then
		cd "$repodir" || exit 1
		git pull --ff-only || exit 1
	else
		git clone "https://git.sr.ht/$repo" || git clone "https://github.com/$repo" || exit 1
		cd "$repodir" || exit 1
	fi
	echo "Building $repo...">&2
	if [ -f config.def.h ]; then
		if [ $FORCE -eq 1 ] || [ ! -f config.h ]; then
			cp -f config.def.h config.h
		else
			echo "(leaving old config.h untouched, this could lead to errors: run sudo ./sxmo-alarm --force if you want to overwrite old config.h files)">&2
		fi
	fi
	if [ "$repo" = "~anjan/mnc" ]; then
		go build mnc.go || exit 1
		sudo cp -f mnc /usr/bin || exit 1
	else
		make || cmake -DCMAKE_INSTALL_PREFIX=/usr . && make . || exit 1
		sudo make PREFIX=/usr install || exit 1
	fi
	if [ "$repo" = "~mil/sxmo-utils" ]; then
        systemctl enable sxmo-pinephone.service
	elif [ "$repo" = "~mil/sxmo-xdm-config" ]; then
		if systemctl is-active --quiet xdm.service; then
           echo "XDM Service is already running.">&2
        else
           echo "Enabling XDM Service.">&2
           systemctl enable xdm.service
        fi
		
	fi
	cd .. || exit 1
done

cd .. || exit 1

# Move config files into correct directories


cp /usr/lib/X11/xdm/Xsession /etc/X11/xdm/
cp /usr/lib/X11/xdm/Xsetup_0 /etc/X11/xdm/Xsetup
cp /usr/lib/X11/xdm/GiveConsole /etc/X11/xdm/
cp /usr/share/X11/xorg.conf.d/90-monitor.conf /etc/X11/xorg.conf.d/



# Housekeeping

sed -i "s/#HandlePowerKey=poweroff/HandlePowerKey=ignore/" /etc/systemd/logind.conf
systemctl enable ModemManager
systemctl enable bluetooth

# E.g. hide the cursor for Xorg

sed -i 's@:0 local /usr/bin/X :0 vt7 -nolisten tcp@:0 local /usr/bin/X :0 vt7 -nocursor -nolisten tcp@g' /etc/X11/xdm/Xservers
sed -i 's@ session will not be started.@ session will not be started \n pkill -9 svkbd @g' /etc/X11/xdm/Xstartup
sed -i 's@if busybox pkill -l > /dev/null; then@if busybox --list | grep pkill -c >0; then@g' /usr/bin/sxmo_common.sh

# Fix sxmo_timezonechange.sh to use command timedatectl

sed -i 's@sudo setup-timezone -z@sudo timedatectl set-timezone@g' /usr/bin/sxmo_timezonechange.sh

# Create required directories in users directory

sudo -u $real_user mkdir -p /home/$real_user/.config/sxmo
sudo -u $real_user mkdir -p /home/$real_user/.config/sxmo/userscripts
sudo -u $real_user mkdir -p /home/$real_user/.config/sxmo/hooks

sudo apt -y remove phoc phosh
sudo reboot
exit 0


